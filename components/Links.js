import Link from "next/link";

export default function Links(props){
    let links = []
    for (let elem in props.links) {
        links.push(<Link href = {props[links][elem].href}> {props[links][elem].children} </Link>)
    }
    return (
            <>
            {links}
            </>
    )
}