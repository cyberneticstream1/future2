export default function Content({children}){
    return(
            <>

            {/* SM ACTION MENU */}
            <div className={"leading-tight font-sans text-white text-center p-3 fixed z-50 text-6xl inset-y-0 my-auto h-96 inset-x-0 mx-auto w-98 md:hidden mx-auto "}>
                <div className={"h-24"}></div>
                <div className={"h-8"}/>
                {children}
            </div>


            {/* MD ACTION MENU */}
            <div className={" leading-tight font-sans text-white text-center p-3 fixed z-50 text-8xl inset-y-0 my-auto h-96 inset-x-0 mx-auto w-120 hidden md:block lg:hidden mx-auto"}>
                <div className={"h-24"}></div>
                <div className={"h-8"}/>
                {children}
            </div>


            </>

    )
}