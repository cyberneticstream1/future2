"use client";
import Link from "next/link";
import React from "react";
import Image from "next/image"
import Box from '@mui/material/Box';
import { DataGridPro } from '@mui/x-data-grid-pro';
import {createTheme, ThemeProvider} from "@mui/material";
import Content from "../../components/content";

const themeSm = createTheme({
    palette: {
        text: {
            primary: "#FFFFFF",
        }
    },
    typography: {
        fontFamily: "var(--font-inter)",
        fontSize: 20,
    },
});

const themeMd = createTheme({
    palette: {
        text: {
            primary: "#FFFFFF",
        }
    },
    typography: {
        fontFamily: "var(--font-inter)",
        fontSize: 45,
    },
});

export default function Page(){
    return (

            <Content children={
                <>
                <div className={"md:hidden"}>
                {/* SM LOG COMPONENT */}
                <Link href = {"/"} className={"opacity-100"}>{"<--"}</Link> <br/>
                <ThemeProvider theme={themeSm}>
                    <div className={""}>
                        <Box sx ={{height: 750, width: "100%"}}>
                            <DataGridPro rows = {[{id: "oct22", amount: "$2,000", status: "complete"}]} columns={[{field: "id", width: 162}, {field: "status", width:162},{field: "amount", width: 162}]}/>
                        </Box>
                    </div>
                </ThemeProvider>
                </div>


                {/* MD LOG COMPONENT */}
                <div className={"hidden md:block"}>
                <Link href = {"/"} className={"opacity-100"}>{"<--"}</Link> <br/>
                <ThemeProvider theme={themeMd}>
                    <div className={""}>
                        <Box sx ={{height: 750, width: "100%"}}>
                            <DataGridPro rows = {[{id: "oct22", amount: "$2,000", status: "complete"}]} columns={[{field: "id", width: 265}, {field: "status", width:265},{field: "amount", width: 265}]}/>
                        </Box>
                    </div>
                </ThemeProvider>
                </div>
                </>
            }/>

            )
}