"use client";
import Link from "next/link";
import React from "react";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import CheckoutForm from "../../components/checkoutForm";
import Content from "../../components/content";
const stripePromise = loadStripe("pk_live_51LlESTC3Ie0MSAM2CQtveok1BNyKHlkw8W0aVunFTMYjMAGi0y6dEaHreNGy0TC4oRkfSMwOkcXUftn0oTlwDaBg00bnHjzls6");

  const appearance = {
            theme: 'flat',
            variables: {
                
              fontFamily: '"var(--font-inter)" ,"Gill Sans", sans-serif',
              fontLineHeight: '1.5',
              borderRadius: '10px',
              colorBackground: '#FFFFFF',
              colorPrimaryText: '#000000',
              colorSecondaryText: '#000000',
                colorBackgroundText: '#000000',
                colorDangerText:"#ff6700",

                colorText: "rgb(0,0,0)",
                colorWarning:"#ff6700",

            },
            rules: {
              '.Block': {
                backgroundColor: 'var(--colorBackground)',
                boxShadow: 'none',
                padding: '12px'
              },
              '.Input': {
                padding: '12px'
              },
              '.Input:disabled, .Input--invalid:disabled': {
                color: 'lightgray'
              },
              '.Tab': {
                padding: '10px 12px 8px 12px',
                border: 'none'
              },
              '.Tab:hover': {
                border: 'none',
                boxShadow: '0px 1px 1px rgba(0, 0, 0, 0.03), 0px 3px 7px rgba(18, 42, 66, 0.04)'
              },
              '.Tab--selected, .Tab--selected:focus, .Tab--selected:hover': {
                border: 'none',
                backgroundColor: '#fff',
                boxShadow: '0 0 0 1.5px var(--colorPrimaryText), 0px 1px 1px rgba(0, 0, 0, 0.03), 0px 3px 7px rgba(18, 42, 66, 0.04)'
              },
              '.Label': {
                fontWeight: '500'
              }
            }
          };


export async function generateStaticParams(){
    return [{id: "dec22"}]
}

export default function Page({params}){
    const [clientSecret, setClientSecret] = React.useState("pi_3Lu1a6C3Ie0MSAM21Ev8pih1_secret_4ppx8yMPRg3ty9KJupaxoUyXb");

    const options = {
        clientSecret,
        appearance,
    };


    return(
            <>

            <Content children = {

                <div className={""}>
                <Link href = {"/"} className={"opacity-100"}>{"<--"}</Link> <br/>
            <Link href = {"/"}>dec 22: $2,300</Link>

            <div className="App">
            {clientSecret && (
                    <Elements options={options} stripe={stripePromise}>
            <CheckoutForm />
                    </Elements>
                    )}
            </div>
            </div>
            } />

            </>
    )
}