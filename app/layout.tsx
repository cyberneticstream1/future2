import "../app/globals.css"
import { Inter } from '@next/font/google';
import localFont from '@next/font/local';
import Map from "../app/map/page.js"

const myFont = localFont({ src: 'GoldmanSans_Th.ttf' });


const inter = Inter({
    variable: '--font-inter',
});

export default function RootLayout({ children }: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en" className={myFont.className}>
        <head>
        </head>
      <body>
          {children}
          <Map/>
      </body>
    </html>
  );
}